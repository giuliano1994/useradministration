package com.mycompany.useradministration.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "roles")
public class RolEntity {

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = "rol_id")
    private Long Id;

     @NotEmpty
     private String nombre;

    @ManyToMany(mappedBy = "roles")
    @JsonIgnore
    private Set<GrupoEntity> grupos = new HashSet<>();

     //Constructor
    public RolEntity() {
    }

    // Getters and Setters

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<GrupoEntity> getGrupos() {
        return grupos;
    }

    public void setGrupos(Set<GrupoEntity> grupos) {
        this.grupos = grupos;
    }

    @Override
    public String toString() {
        return "RolEntity{" +
                "Id=" + Id +
                ", nombre='" + nombre + '\'' +
                ", grupos=" + grupos.toString() +
                '}';
    }
}

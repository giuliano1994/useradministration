package com.mycompany.useradministration.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "grupos")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class GrupoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "grupo_id")
    private Long id;

    private String nombre;

    @ManyToMany(mappedBy = "grupos")
    @JsonIgnore
    private Set<UsuarioEntity> usuarios = new HashSet<>();

    @JoinTable(
            name = "rol_grupo", // Nombre de la tabla de asociación (debe ser el mismo que en la clase RolEntity)
            joinColumns = @JoinColumn(name = "FK_GRUPOS", referencedColumnName = "grupo_id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "FK_REL", referencedColumnName = "rol_id", nullable = false)
    )
    @JsonIgnore
    @ManyToMany(cascade = CascadeType.PERSIST)
    private Set<RolEntity> roles = new HashSet<>();




    // Constructor
    public GrupoEntity() {
    }


    public Set<UsuarioEntity> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Set<UsuarioEntity> usuarios) {
        this.usuarios = usuarios;
    }

    public void addRol(RolEntity rol) {
        if (this.roles == null){
            this.roles = new HashSet<>();
        }
        if (!this.roles.contains(rol)) {
            roles.add(rol);

        }
    }

    public void removeRol(RolEntity rol) {
        if (roles.contains(rol)) {
            roles.remove(rol);

        }
    }

    // Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<RolEntity> getRoles() {
        return roles;
    }

    public void setRoles(Set<RolEntity> roles) {
        this.roles = roles;
    }




    @Override
    public String toString() {
        return "GrupoEntity{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", roles=" + roles.toString() +
                '}';
    }
}

package com.mycompany.useradministration.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import org.springframework.format.annotation.DateTimeFormat;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table (name = "usuarios")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UsuarioEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotEmpty
    @NotBlank
    private String nombre;

    @NotEmpty
    @NotBlank
    private String apellido;
    @NotEmpty
    @Size(min = 8, max = 30)
    private String contrasenia;
    @Column(name = "email", unique = true)
    private String email;
    @NotEmpty
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate alta;

    @JoinTable(
            name = "usuario_grupo", // Nombre de la tabla de asociación (debe ser el mismo que en la clase RolEntity)
            joinColumns = @JoinColumn(name = "FK_USUARIO", referencedColumnName = "id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "FK_GRUPO", referencedColumnName = "grupo_id", nullable = false)
    )
    @JsonIgnore
    @ManyToMany(cascade = CascadeType.PERSIST)
    private Set<GrupoEntity> grupos = new HashSet<>();

    // Constructor
    public UsuarioEntity() {
    }

    // Getters y Setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getAlta() {
        return alta;
    }

    public void setAlta(LocalDate alta) {
        this.alta = alta;
    }

    public Set<GrupoEntity> getGrupo() {
        return grupos;
    }

    public void setGrupo(Set<GrupoEntity> grupo) {
        this.grupos = grupo;
    }


    @Override
    public String toString() {
        return "UsuarioEntity{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", contraseña='" + contrasenia + '\'' +
                ", mail='" + email + '\'' +
                ", alta=" + alta +
                ", rol=" + grupos +
                '}';
    }




}


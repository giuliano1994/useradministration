package com.mycompany.useradministration.response;

public class DeleteUsuario {
    private boolean exitoso;
    private String mensaje;

    public DeleteUsuario() {
    }

    public boolean isExitoso() {
        return exitoso;
    }

    public void setExitoso(boolean exitoso) {
        this.exitoso = exitoso;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}

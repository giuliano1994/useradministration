package com.mycompany.useradministration.response;

import com.mycompany.useradministration.entity.UsuarioEntity;

public class ObtenerUsuarioId {
    private boolean exitoso;
    private String mensaje;
    private UsuarioEntity usuario;

    public ObtenerUsuarioId() {
    }


    public boolean isExitoso() {
        return exitoso;
    }

    public void setExitoso(boolean exitoso) {
        this.exitoso = exitoso;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public UsuarioEntity getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioEntity usuario) {
        this.usuario = usuario;
    }
}

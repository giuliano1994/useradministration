package com.mycompany.useradministration.mapper;

import com.mycompany.useradministration.dto.grupoDTO.GrupoBaseDTO;
import com.mycompany.useradministration.dto.usuarioDTO.UsuarioBaseDTO;
import com.mycompany.useradministration.dto.usuarioDTO.UsuarioCompletoDTO;
import com.mycompany.useradministration.entity.GrupoEntity;
import com.mycompany.useradministration.entity.UsuarioEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class UsuarioMapper {

    private final ModelMapper modelMapper;

    public UsuarioMapper (ModelMapper modelMapper){
        this.modelMapper = modelMapper;
    }

    public UsuarioBaseDTO mapUsuarioBaseToDTO(UsuarioEntity usuarioEntity) {
        return modelMapper.map(usuarioEntity, UsuarioBaseDTO.class);
    }
    public UsuarioCompletoDTO mapUsuarioCOmpletoTODTO (UsuarioEntity usuarioEntity){
        return modelMapper.map(usuarioEntity, UsuarioCompletoDTO.class);
    }

    public GrupoBaseDTO mapGrupoBaseToDTO(GrupoEntity grupoEntity) {
        return modelMapper.map(grupoEntity, GrupoBaseDTO.class);
    }
}

package com.mycompany.useradministration.repository;

import com.mycompany.useradministration.entity.UsuarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Long> {

    @Query ("SELECT COUNT(usu) > 0 FROM UsuarioEntity usu WHERE usu.email = :email")
    boolean existsByEmail(@Param("email") String email);

    @Query ("SELECT usu FROM UsuarioEntity usu WHERE usu.email = :email")
    UsuarioEntity getReferenceByEmail(String email);
}

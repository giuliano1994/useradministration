package com.mycompany.useradministration.repository;

import com.mycompany.useradministration.entity.GrupoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface GrupoRepository  extends JpaRepository <GrupoEntity, Long> {


}

package com.mycompany.useradministration.dto.usuarioDTO;

public class UsuarioLoginDTO {

    private final String email;
    private final String contrasenia;

    public UsuarioLoginDTO(String email, String contrasenia) {
        this.email = email;
        this.contrasenia = contrasenia;
    }


    public String getEmail() {
        return email;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    @Override
    public String toString() {
        return "UsuarioLoginDTO{" +
                "mail='" + email + '\'' +
                ", contraseña='" + contrasenia + '\'' +
                '}';
    }
}

package com.mycompany.useradministration.dto.usuarioDTO;

import com.mycompany.useradministration.dto.grupoDTO.GrupoBaseDTO;
import com.mycompany.useradministration.dto.rolDTO.RolDTO;

import java.time.LocalDate;
import java.util.List;

public class UsuarioCompletoGrupoRolDTO extends UsuarioCompletoDTO{

    private List<GrupoBaseDTO> grupos;
    private List<RolDTO> rol;


    public UsuarioCompletoGrupoRolDTO() {
    }

    // Constructor

    public UsuarioCompletoGrupoRolDTO(List<GrupoBaseDTO> grupos, List<RolDTO> rol) {
        this.grupos = grupos;
        this.rol = rol;
    }

    public UsuarioCompletoGrupoRolDTO(Long id, String nombre, String apellido, String mail, LocalDate alta) {
        super(id, nombre, apellido, mail, alta);
    }

    public List<GrupoBaseDTO> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<GrupoBaseDTO> grupos) {
        this.grupos = grupos;
    }

    public List<RolDTO> getRol() {
        return rol;
    }

    public void setRol(List<RolDTO> rol) {
        this.rol = rol;
    }
}

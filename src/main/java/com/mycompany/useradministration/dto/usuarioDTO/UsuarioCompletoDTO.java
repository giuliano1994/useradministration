package com.mycompany.useradministration.dto.usuarioDTO;

import java.time.LocalDate;

public class UsuarioCompletoDTO extends UsuarioBaseDTO{

    private String email;
    private LocalDate alta;


    public UsuarioCompletoDTO() {

    }

    public UsuarioCompletoDTO(Long id, String nombre, String apellido, String mail, LocalDate alta) {
        super(id, nombre, apellido);
        this.email = mail;
        this.alta = alta;
    }

    public String getMail() {
        return email;
    }

    public void setMail(String mail) {
        this.email = mail;
    }

    public LocalDate getAlta() {
        return alta;
    }

    public void setAlta(LocalDate alta) {
        this.alta = alta;
    }
}

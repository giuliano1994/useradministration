package com.mycompany.useradministration.dto.grupoDTO;

import com.mycompany.useradministration.dto.rolDTO.RolDTO;

import java.util.List;

public class GrupoRolDTO extends GrupoBaseDTO{

    private List<RolDTO> roles;

    public GrupoRolDTO() {
    }

    public GrupoRolDTO(Long id, String nombre, List<RolDTO> roles) {
        super(id, nombre);
        this.roles = roles;
    }

    public List<RolDTO> getRoles() {
        return roles;
    }

    public void setRoles(List<RolDTO> roles) {
        this.roles = roles;
    }
}

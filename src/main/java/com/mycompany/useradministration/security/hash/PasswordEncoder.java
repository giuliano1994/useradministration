package com.mycompany.useradministration.security.hash;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class PasswordEncoder {
    BCryptPasswordEncoder encoder;
    public PasswordEncoder(){
        encoder = new BCryptPasswordEncoder();
    }

    public String encoderPass (String pass){
        return encoder.encode(pass);
    }

    public boolean matchPass (String pass, String passEncoder) {
        return encoder.matches(pass, passEncoder);
    }


}

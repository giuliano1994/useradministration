package com.mycompany.useradministration.service.iservice;

import com.mycompany.useradministration.entity.GrupoEntity;
import com.mycompany.useradministration.entity.UsuarioEntity;

import java.util.List;
import java.util.Set;

public interface IUsuarioService {

    UsuarioEntity crearUsuario(UsuarioEntity usuario);
    List<UsuarioEntity> obtenerTodosLosUsuarios();
    UsuarioEntity obtenerUsuarioPorId(Long id);
    UsuarioEntity eliminarUsuarioPorId(Long id);



    // UsuarioDTO eliminarUsuario(UsuarioEntity usuario);
    UsuarioEntity editarUsuario(Long id, UsuarioEntity usuario);

    GrupoEntity agregarGrupoAUsuario(Long grupoId, Long usuarioId);

    GrupoEntity quitarGrupoDelUsuario(Long grupoId, Long usuarioId);

    Set<GrupoEntity> obtenerGruposDelUsuario(Long grupoId);

}

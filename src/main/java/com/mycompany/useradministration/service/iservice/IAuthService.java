package com.mycompany.useradministration.service.iservice;

import com.mycompany.useradministration.dto.usuarioDTO.UsuarioLoginDTO;

public interface IAuthService {

    String authenticate (UsuarioLoginDTO usuarioLogin);
}

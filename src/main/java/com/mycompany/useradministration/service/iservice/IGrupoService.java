package com.mycompany.useradministration.service.iservice;

import com.mycompany.useradministration.entity.GrupoEntity;
import com.mycompany.useradministration.entity.RolEntity;

import java.util.List;
import java.util.Set;

public interface IGrupoService {
    GrupoEntity crearGrupo(GrupoEntity grupo);
    List<GrupoEntity> obtenerTodosLosGrupos();
    GrupoEntity obtenerGrupoPorId(Long id);
    boolean eliminarGrupoPorId(Long id);
    boolean eliminarGrupo(GrupoEntity grupo);
    GrupoEntity editarGrupo(Long id, GrupoEntity grupo);

    GrupoEntity agregarRolAGrupo(Long grupoId, Long rolId);

    GrupoEntity quitarRolDeGrupo(Long grupoId, Long rolId);

    Set<RolEntity> obtenerRolesDelGrupo(Long grupoId);

}

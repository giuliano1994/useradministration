package com.mycompany.useradministration.service.iservice;

import com.mycompany.useradministration.entity.RolEntity;

import java.util.List;

public interface IRolService {

    RolEntity crearRol(RolEntity rol);
    List<RolEntity> obtenerTodosLosRoles();
    RolEntity obtenerRolPorId(Long id);
    boolean eliminarRolPorId(Long id);
    boolean eliminarRol(RolEntity rol);
    RolEntity editarRol(Long id, RolEntity rol);

}

package com.mycompany.useradministration.service.service;

import com.mycompany.useradministration.entity.GrupoEntity;
import com.mycompany.useradministration.entity.RolEntity;
import com.mycompany.useradministration.repository.GrupoRepository;
import com.mycompany.useradministration.repository.RolRepository;
import com.mycompany.useradministration.service.iservice.IGrupoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class GrupoService implements IGrupoService {

    private final GrupoRepository grupoRepository;
    private final RolRepository rolRepository;

    @Autowired
    public GrupoService(GrupoRepository grupoRepository, RolRepository rolRepository) {
        this.grupoRepository = grupoRepository;
        this.rolRepository = rolRepository;
    }

    @Override
    public GrupoEntity crearGrupo(GrupoEntity grupo) {
        return grupoRepository.save(grupo);
    };

    @Override
    public List<GrupoEntity> obtenerTodosLosGrupos() {
        return grupoRepository.findAll();
    };

    @Override
    public GrupoEntity obtenerGrupoPorId(Long id) {

        if(grupoRepository.existsById(id)){
            return grupoRepository.getReferenceById(id);
        }else{
            return null;
        }
    };

    @Override
    public boolean eliminarGrupoPorId(Long id) {
        if(grupoRepository.existsById(id)){
            grupoRepository.deleteById(id);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean eliminarGrupo(GrupoEntity grupo) {
        if(grupoRepository.existsById(grupo.getId())){
            grupoRepository.deleteById(grupo.getId());
            return true;
        }else{
            return false;
        }
    }

    @Override
    public GrupoEntity editarGrupo(Long id, GrupoEntity grupo) {

        if(grupoRepository.existsById(id)){
            GrupoEntity grupoExistente = obtenerGrupoPorId(id);
            grupoExistente.setNombre(grupo.getNombre());
            grupoRepository.save(grupoExistente);
            return grupoExistente;
        }else{
            return null;
        }
    }

    @Override
    public GrupoEntity agregarRolAGrupo(Long grupoId, Long rolId) {

        if(grupoRepository.existsById(grupoId) && rolRepository.existsById(rolId)){
            GrupoEntity grupo = grupoRepository.getReferenceById(grupoId);
            RolEntity rol = rolRepository.getReferenceById(rolId);

            grupo.getRoles().add(rol);
            rol.getGrupos().add(grupo);

            grupoRepository.save(grupo);
            rolRepository.save(rol);

            return grupo;
        }
        return null;
    }

    @Override
    public GrupoEntity quitarRolDeGrupo(Long grupoId, Long rolId) {

        if(grupoRepository.existsById(grupoId) && rolRepository.existsById(rolId)){
            System.out.println("entre");
            GrupoEntity grupo = grupoRepository.getReferenceById(grupoId);
            RolEntity rol = rolRepository.getReferenceById(rolId);
            if (grupo.getRoles().contains(rol)){

                grupo.removeRol(rol);
                grupoRepository.save(grupo);
                return grupo;
            }
        }
        return null;
    }

    @Override
    public Set<RolEntity> obtenerRolesDelGrupo(Long grupoId) {
        GrupoEntity grupo = grupoRepository.getReferenceById(grupoId);
        return grupo.getRoles();
    }
}


package com.mycompany.useradministration.service.service;

import com.mycompany.useradministration.entity.GrupoEntity;
import com.mycompany.useradministration.entity.UsuarioEntity;
import com.mycompany.useradministration.repository.GrupoRepository;
import com.mycompany.useradministration.repository.UsuarioRepository;
import com.mycompany.useradministration.security.hash.PasswordEncoder;
import com.mycompany.useradministration.service.iservice.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class UsuarioService implements IUsuarioService {

    private final UsuarioRepository repository;
    private final GrupoRepository grupoRepository;
    private final PasswordEncoder encoder;

    @Autowired
    public UsuarioService(UsuarioRepository usuarioRepository, GrupoRepository grupoRepository, PasswordEncoder encoder) {
        this.repository = usuarioRepository;
        this.grupoRepository = grupoRepository;
        this.encoder = encoder;

    }



    private boolean verificarDatos(UsuarioEntity usuario){
        return (!usuario.getNombre().isEmpty()) &&
                (!usuario.getApellido().isEmpty()) &&
                (usuario.getContrasenia().length() >= 8 && usuario.getContrasenia().length() <= 50 &&
                        !repository.existsByEmail(usuario.getEmail()));
    }

    @Override
    public UsuarioEntity crearUsuario(UsuarioEntity usuario) {
        if (verificarDatos(usuario)){
            usuario.setContrasenia(encoder.encoderPass(usuario.getContrasenia()));
            repository.save(usuario);
            return usuario;

        }else{
            return null;
        }
    }

    @Override
    public List<UsuarioEntity> obtenerTodosLosUsuarios() {
        return repository.findAll();
    }

    @Override
    public UsuarioEntity obtenerUsuarioPorId(Long id) {
        if (repository.existsById(id)){
            return repository.getReferenceById(id);
        }
        return null;
    }

    @Override
    public UsuarioEntity eliminarUsuarioPorId(Long id) {
        if (repository.existsById(id)) {
            UsuarioEntity usuarioborrado = repository.getReferenceById(id);
            repository.deleteById(id);
            return usuarioborrado;
        }
        return null;
    }


    @Override
    public UsuarioEntity editarUsuario(Long id, UsuarioEntity usuarioActualizado) {
        if (repository.existsById(id)){
            UsuarioEntity usuarioExistente = repository.getReferenceById(id);
            usuarioExistente.setNombre(usuarioActualizado.getNombre());
            usuarioExistente.setApellido(usuarioActualizado.getApellido());
            usuarioExistente.setContrasenia(usuarioActualizado.getContrasenia());
            usuarioExistente.setEmail(usuarioActualizado.getEmail());
            usuarioExistente.setAlta(usuarioActualizado.getAlta());
            usuarioExistente.setGrupo(usuarioActualizado.getGrupo());
            repository.save(usuarioExistente);
            return usuarioExistente;
        }
        return null;
    }

    @Override
    public GrupoEntity agregarGrupoAUsuario(Long usuarioId, Long grupoId) {
        if (repository.existsById(usuarioId) && grupoRepository.existsById(grupoId)){
            UsuarioEntity usuario = repository.getReferenceById(usuarioId);
            GrupoEntity grupo = grupoRepository.getReferenceById(grupoId);

            usuario.getGrupo().add(grupo);
            grupo.getUsuarios().add(usuario);

            repository.save(usuario);
            grupoRepository.save(grupo);

            return grupo;
        }
        return null;
    }

    @Override
    public GrupoEntity quitarGrupoDelUsuario(Long usuarioId, Long grupoId) {
        if (repository.existsById(usuarioId) && grupoRepository.existsById(grupoId)){
            UsuarioEntity usuario = repository.getReferenceById(usuarioId);
            GrupoEntity grupo = grupoRepository.getReferenceById(grupoId);
            usuario.getGrupo().remove(grupo);
            grupo.getUsuarios().remove(usuario);

            repository.save(usuario);
            grupoRepository.save(grupo);

            return grupo;
        }
        return null;
    }

    @Override
    public Set<GrupoEntity> obtenerGruposDelUsuario(Long usuarioId) {
        if (repository.existsById(usuarioId)){
            UsuarioEntity usuario = repository.getReferenceById(usuarioId);
            return usuario.getGrupo();
        }else{
            return null;
        }

    }


}

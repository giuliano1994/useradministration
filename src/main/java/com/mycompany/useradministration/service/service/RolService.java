package com.mycompany.useradministration.service.service;

import com.mycompany.useradministration.entity.GrupoEntity;
import com.mycompany.useradministration.entity.RolEntity;
import com.mycompany.useradministration.repository.RolRepository;
import com.mycompany.useradministration.service.iservice.IRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class RolService implements IRolService {

    private RolRepository repository;

    @Autowired
    public RolService (RolRepository repository){
        this.repository = repository;
    }

    @Override
    public RolEntity crearRol(RolEntity rol) {
        return repository.save(rol);
    }

    @Override
    public List<RolEntity> obtenerTodosLosRoles() {
        return repository.findAll();
    }

    @Override
    public RolEntity obtenerRolPorId(Long id) {
        if(repository.existsById(id)){
            return repository.getReferenceById(id);
        }else{
            return null;
        }
    }

    @Override
    public boolean eliminarRolPorId(Long id) {
        if(repository.existsById(id)){
            repository.deleteById(id);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean eliminarRol(RolEntity rol) {
        if(repository.existsById(rol.getId())){
            repository.deleteById(rol.getId());
            return true;
        }else{
            return false;
        }
    }

    @Override
    public RolEntity editarRol(Long id, RolEntity rol) {
        if(repository.existsById(id)){
            RolEntity rolExistente = obtenerRolPorId(id);
            rolExistente.setNombre(rol.getNombre());
            return rolExistente;
        }else{
            return null;
        }
    }

    public Set<GrupoEntity> obtenerGruposDelRol(Long rolId) {
        RolEntity rol = repository.getById(rolId);
        return rol.getGrupos();
    }
}

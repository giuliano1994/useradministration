package com.mycompany.useradministration.service.service;

import com.mycompany.useradministration.dto.usuarioDTO.UsuarioLoginDTO;
import com.mycompany.useradministration.entity.UsuarioEntity;
import com.mycompany.useradministration.security.hash.PasswordEncoder;
import com.mycompany.useradministration.security.token.JwtToken;
import com.mycompany.useradministration.repository.UsuarioRepository;
import com.mycompany.useradministration.service.iservice.IAuthService;
import org.springframework.stereotype.Service;

@Service
public class AuthService implements IAuthService {

    private final UsuarioRepository usuarioRepository;
    private final JwtToken jwtToken;
    private final PasswordEncoder encoder;

    public AuthService (UsuarioRepository usuarioRepository, PasswordEncoder encoder, JwtToken jwtToken){
        this.usuarioRepository = usuarioRepository;
        this.encoder = encoder;
        this.jwtToken = jwtToken;
    }
    @Override
    public String authenticate(UsuarioLoginDTO usuarioLogin) {
        if (usuarioRepository.existsByEmail(usuarioLogin.getEmail())){
            UsuarioEntity usuarioExistente = usuarioRepository.getReferenceByEmail(usuarioLogin.getEmail());
            if (encoder.matchPass(usuarioLogin.getContrasenia(), usuarioExistente.getContrasenia())){
                return jwtToken.generateToken(usuarioLogin.getContrasenia());
            }
        }
        return null;
    }
}

package com.mycompany.useradministration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserAdministrationPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserAdministrationPortalApplication.class, args);
	}

}

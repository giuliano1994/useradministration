package com.mycompany.useradministration.controller;

import com.mycompany.useradministration.dto.grupoDTO.GrupoBaseDTO;
import com.mycompany.useradministration.dto.usuarioDTO.UsuarioBaseDTO;
import com.mycompany.useradministration.entity.GrupoEntity;
import com.mycompany.useradministration.entity.UsuarioEntity;
import com.mycompany.useradministration.mapper.UsuarioMapper;
import com.mycompany.useradministration.service.service.UsuarioService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {
    private final UsuarioService service;
    private final UsuarioMapper usuarioMapper;

    private static final Logger logger = LogManager.getLogger(UsuarioController.class);


    @Autowired
    public UsuarioController(UsuarioService service, UsuarioMapper usuarioMapper){

        this.service = service;
        this.usuarioMapper = usuarioMapper;
    }

    @GetMapping("/{rol}")
    public ResponseEntity<List<UsuarioBaseDTO>>obtenerTodosLosUsuarios(@PathVariable String rol) {
        List<UsuarioEntity> usuarios = service.obtenerTodosLosUsuarios();
        List<UsuarioBaseDTO> usuariosMappeados = new ArrayList<>();
        if (rol.equals("ADMIN")){
            for (UsuarioEntity usuario : usuarios){
                usuariosMappeados.add(usuarioMapper.mapUsuarioCOmpletoTODTO(usuario));
            }
            return ResponseEntity.ok(usuariosMappeados);
        }
        if (rol.equals("BASE")){
            for (UsuarioEntity usuario : usuarios){
                usuariosMappeados.add(usuarioMapper.mapUsuarioBaseToDTO(usuario));
            }
            return ResponseEntity.ok(usuariosMappeados);
        }
        logger.info(String.format ("No se ingreso un usuario permitido, su rol es: "), rol);
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/{id}/{rol}")
    public ResponseEntity<UsuarioBaseDTO> obtenerUsuarioPorId (@PathVariable Long id, @PathVariable String rol){
        UsuarioEntity usuarioId = service.obtenerUsuarioPorId(id);

        if (rol.equals("ADMIN")){

            return  ResponseEntity.ok(usuarioMapper.mapUsuarioCOmpletoTODTO(usuarioId));
        }
        if (rol.equals("BASE")){
            return  ResponseEntity.ok(usuarioMapper.mapUsuarioBaseToDTO(usuarioId));
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping
    public ResponseEntity<UsuarioEntity> crearUsuario (@RequestBody  UsuarioEntity usuario){
        UsuarioEntity crearUsusrio = service.crearUsuario(usuario);
        if (crearUsusrio != null){
            return ResponseEntity.status(HttpStatus.CREATED).body(crearUsusrio);
        }else{
            logger.error(String.format("no se genero el nuevo usuario, datos erroneos: "), usuario);
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}/{rol}")
    public ResponseEntity<UsuarioBaseDTO> eliminarPorID (@PathVariable Long id, @PathVariable String rol){

        UsuarioEntity deleteUsuario = service.eliminarUsuarioPorId(id);
        if (deleteUsuario != null){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();

        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<UsuarioBaseDTO> editarUsuario (@PathVariable Long id, @RequestBody UsuarioEntity usuario){
        UsuarioEntity resp = service.editarUsuario(id, usuario);
        if ((resp!= null)){
            return  ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{usuarioId}/agregarGrupo/{grupoId}")
    public ResponseEntity<GrupoBaseDTO> agregarGrupoAUsuario(@PathVariable Long usuarioId, @PathVariable Long grupoId) {
        GrupoEntity grupo = service.agregarGrupoAUsuario(usuarioId, grupoId);
        if (grupo != null){
            return ResponseEntity.ok(usuarioMapper.mapGrupoBaseToDTO(grupo));
        }
        else{
            return ResponseEntity.badRequest().build();
        }

    }

    @DeleteMapping("/{usuarioId}/quitarGrupo/{grupoId}")
    public ResponseEntity<GrupoBaseDTO> quitarGrupoDelUsuario(@PathVariable Long usuarioId, @PathVariable Long grupoId) {
        GrupoEntity grupo = service.quitarGrupoDelUsuario(usuarioId, grupoId);
        if (grupo != null){
            return ResponseEntity.ok(usuarioMapper.mapGrupoBaseToDTO(grupo));
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/{usuarioId}/grupos")
    public ResponseEntity<Set<GrupoBaseDTO>> obtenerGrupoDelUsuario(@PathVariable Long usuarioId) {
        Set<GrupoEntity> grupos = service.obtenerGruposDelUsuario(usuarioId);
        Set<GrupoBaseDTO> gruposDto = new HashSet<>();
        for(GrupoEntity grupo : grupos){
            gruposDto.add(usuarioMapper.mapGrupoBaseToDTO(grupo));
        }
        return ResponseEntity.ok(gruposDto);
    }

  //  @Target(ElementType.METHOD)
  //  @Retention(RetentionPolicy.RUNTIME)
  //  public @interface TokenRequired {

  //  }
}

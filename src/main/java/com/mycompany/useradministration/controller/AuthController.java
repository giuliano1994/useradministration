package com.mycompany.useradministration.controller;

import com.mycompany.useradministration.dto.usuarioDTO.UsuarioLoginDTO;
import com.mycompany.useradministration.service.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    public String tokenGenerado;

    private final AuthService service;
    @Autowired
    public AuthController(AuthService service){
        this.service = service;
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody UsuarioLoginDTO usuarioLogin) {

        tokenGenerado = service.authenticate(usuarioLogin);

        if (tokenGenerado != null){
            return ResponseEntity.ok(tokenGenerado);
        }else{
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Credencial incorrecta");
        }
    }


}

package com.mycompany.useradministration.controller;

import com.mycompany.useradministration.entity.RolEntity;
import com.mycompany.useradministration.service.service.RolService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/roles")
public class RolController {
        RolService service;

        public RolController (RolService service){
            this.service = service;
        }

        @GetMapping
        public ResponseEntity<List<RolEntity>> obtenerTodosLosRoles() {
            List<RolEntity> roles = service.obtenerTodosLosRoles();
            return ResponseEntity.ok(roles);
        }

        @GetMapping("/{id}")
        public ResponseEntity<RolEntity> obtenerRolPorId (@PathVariable Long id){
            RolEntity rol = service.obtenerRolPorId(id);
            if (rol != null){
                return ResponseEntity.ok(rol);
            }else{
                return ResponseEntity.notFound().build();
            }
        }

        @PostMapping
        public ResponseEntity<RolEntity> crearRol (@RequestBody RolEntity rol){
            RolEntity rolCreado = service.crearRol(rol);
            if (rolCreado != null){
                return ResponseEntity.status(HttpStatus.CREATED).body(rolCreado);
            }else{
                return ResponseEntity.notFound().build();
            }
        }

        @DeleteMapping
        public ResponseEntity<RolEntity> eliminarRol (@RequestBody RolEntity rol){
            boolean deleteRol = service.eliminarRol(rol);
            if (deleteRol){
                return ResponseEntity.noContent().build();
            }else{
                return ResponseEntity.notFound().build();
            }
        }

        @DeleteMapping("/{id}")
        public ResponseEntity<RolEntity> eliminarRolPorID (@PathVariable Long id){

            boolean deleteRol = service.eliminarRolPorId(id);
            if (deleteRol){
                return ResponseEntity.ok().build();
            }else{
                return ResponseEntity.notFound().build();

            }
        }

        @PutMapping("/{id}")
        public ResponseEntity<RolEntity> editarRol (@PathVariable Long id, @RequestBody RolEntity rol){
            RolEntity rolEditado = service.editarRol(id, rol);
            if ((rolEditado != null)){
                return  ResponseEntity.ok().build();
            }else{
                return ResponseEntity.notFound().build();
            }
        }
}

package com.mycompany.useradministration.controller;

import com.mycompany.useradministration.entity.GrupoEntity;
import com.mycompany.useradministration.entity.RolEntity;
import com.mycompany.useradministration.service.service.GrupoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/grupos")
public class GrupoController {

    GrupoService service;

    @Autowired
    public GrupoController (GrupoService service){
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<GrupoEntity>> obtenerTodosLosGrupos() {
        List<GrupoEntity> grupos = service.obtenerTodosLosGrupos();
        return ResponseEntity.ok(grupos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GrupoEntity> obtenerGrupoPorId (@PathVariable Long id){
        GrupoEntity grupo = service.obtenerGrupoPorId(id);

        if (grupo != null){
            return ResponseEntity.ok(grupo);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<GrupoEntity> crearGrupo (@RequestBody GrupoEntity grupo){
        GrupoEntity grupoCreado = service.crearGrupo(grupo);
        if (grupoCreado != null){
            return ResponseEntity.status(HttpStatus.CREATED).body(grupo);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping
    public ResponseEntity<GrupoEntity> eliminarGrupo (@RequestBody GrupoEntity grupo){
        boolean deleteGrupo = service.eliminarGrupo(grupo);
        if (deleteGrupo){
            return ResponseEntity.noContent().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<GrupoEntity> eliminarPorID (@PathVariable Long id){

        boolean deleteGrupo = service.eliminarGrupoPorId(id);
        if (deleteGrupo){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();

        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<GrupoEntity> editarGrupo (@PathVariable Long id, @RequestBody GrupoEntity grupo){
        GrupoEntity grupoEditado = service.editarGrupo(id, grupo);
        if ((grupoEditado != null)){
            return  ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }



    @PutMapping("/{grupoId}/agregarRol/{rolId}")
    public ResponseEntity<GrupoEntity> agregarRolAGrupo(@PathVariable Long grupoId, @PathVariable Long rolId) {
        GrupoEntity grupo = service.agregarRolAGrupo(grupoId, rolId);
        if (grupo != null){
            return ResponseEntity.ok(grupo);
        }
        else{
            return ResponseEntity.notFound().build();
        }

    }


    @DeleteMapping("/{grupoId}/quitarRol/{rolId}")
    public ResponseEntity<GrupoEntity> quitarRolDelGrupo(@PathVariable Long grupoId, @PathVariable Long rolId) {
        GrupoEntity grupo = service.quitarRolDeGrupo(grupoId, rolId);
        if (grupo != null){
            return ResponseEntity.ok(grupo);
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/{grupoId}/roles")
    public ResponseEntity<Set<RolEntity>> obtenerRolesDelGrupo(@PathVariable Long grupoId) {
        Set<RolEntity> roles = service.obtenerRolesDelGrupo(grupoId);
        return ResponseEntity.ok(roles);
    }
}
